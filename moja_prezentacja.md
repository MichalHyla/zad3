---
author: Michał Hyła
title: Prezentacja nt. FC Barcelony
date: 07.11.2021
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Krótka historia klubu 

**Początki** - 29 listopada 1899 pasjonujący się futbolem 22-letni Szwajcar Hans Gamper wraz z grupą przyjaciół zebranych w sali gimnastycznej *Gimnasio Solé*, przy barcelońskiej ulicy La Rambla utworzył Foot-Ball Club Barcelona.


## Stadion

Barcelona rozgrywa swoje mecze na stadionie **Camp Nou**. Trybuny mieszczą 99 354 osób, czyniąc go największym piłkarskim stadionem w Europie



Stadion oddano do użytku 24 września 1957r.


## Wyniki w sezonie 2021/2022

|     Rozgrywki      | Rozegrane mecze | Wygrane | Remisy | Porażki | Różnica bramek |
| :----------------: | :---: | :---: | :---: | :---: | :---: |
|      La Liga       |       12        | 4       | 5      |    3    |       +4       |
| Liga Mistrzów UEFA |        4        | 2       | 0      |    2    |       -4       |



## Wzór na obliczenie zapełnienia stadionu

n - liczba osób obecnych na meczu

c - współczynnik zapełnienia stadionu

 $$c=\frac{n}{99354}$$
 

 
## Bibliografia
 
 [wikipedia](https://pl.wikipedia.org/wiki/FC_Barcelona)
